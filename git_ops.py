import argparse
import gitlab
from semantic_version import Version

class GitLabManager:
    def __init__(self, api_url, private_token):
        self.gl = gitlab.Gitlab(api_url, private_token=private_token)

    def get_latest_tag(self, project_id):
        project = self.gl.projects.get(project_id)
        tags = project.tags.list(get_all=True)
        def get_version(tag):
            try:
                return Version(tag.name.lstrip("v"))
            except ValueError:
                return Version("0.0.0")

        sorted_tags = sorted(tags, key=lambda tag: get_version(tag), reverse=False)
        return [tag.name for tag in sorted_tags][-1]

    def search_tag(self, project_id, search_pattern):
        project = self.gl.projects.get(project_id)
        tags = project.tags.list(search="^"+search_pattern, order='name', get_all=True)
        def get_version(tag):
            try:
                return Version(tag.name.lstrip("v"))
            except ValueError:
                return Version("0.0.0")

        sorted_tags = sorted(tags, key=lambda tag: get_version(tag), reverse=False)
        return [tag.name for tag in sorted_tags][-1]
    
    def create_tag(self, project_id, tag_name, ref, message=None):
        project = self.gl.projects.get(project_id)
        tag = project.tags.create({'tag_name': tag_name, 'ref': ref, 'message': message})
        return tag.name


def parse_arguments():
    parser = argparse.ArgumentParser(description="GitLab Manager")

    parser.add_argument("--api_url", required=True, help="GitLab API URL")
    parser.add_argument("--private_token", required=True, help="GitLab Private Token")
    parser.add_argument("--project_id", required=True, help="GitLab Project ID")

    subparsers = parser.add_subparsers(dest="command", help="Available commands")

    search_tag_parser = subparsers.add_parser("search_tag", help="Search tags")
    search_tag_parser.add_argument("search_pattern", help="Pattern to search for")

    create_tag_parser = subparsers.add_parser("create_tag", help="Create a new tag")
    create_tag_parser.add_argument("tag_name", help="Name of the new tag")
    create_tag_parser.add_argument("ref_name", help="Reference name for the tag")
    create_tag_parser.add_argument("--message", help="Optional message for the tag")
    
    get_latest_tag_parser = subparsers.add_parser("get_latest_tag", help="Get Latest tag")
    return parser.parse_args()

def main():
    args = parse_arguments()
    gitlab_manager = GitLabManager(args.api_url, args.private_token)

    if args.command == "get_latest_tag":
        tags_list = gitlab_manager.get_latest_tag(args.project_id)
        print(tags_list)

    elif args.command == "search_tag":
        matching_tags = gitlab_manager.search_tag(args.project_id, args.search_pattern)
        print(matching_tags)

    elif args.command == "create_tag":
        created_tag = gitlab_manager.create_tag(args.project_id, args.tag_name, args.ref_name, args.message)
        print(created_tag)

if __name__ == "__main__":
    main()
