import re
import gitlab
import argparse

def get_gitlab_branches(api_url, private_token, project_id):
    gl = gitlab.Gitlab(api_url, private_token=private_token, api_version=4)
    project = gl.projects.get(project_id)
    return [branch.name for branch in project.branches.list()]

def find_matching_branches(branch_names, pattern):
    return [branch for branch in branch_names if re.match(pattern, branch)]

def get_latest_minor_branch(strings):
    max_minor_version = max([int(re.match(r"bcp-\d+\.(\d+)\.x", s).group(1)) for s in strings], default=0)
    latest_branch = next((s for s in strings if re.match(r"bcp-2\.{}\.x".format(max_minor_version), s)), None)

    print(f"Largest branch {latest_branch}")

    if latest_branch:
        new_branch = f"bcp-2.{max_minor_version + 1}.x"
        print(f"Creating a new branch {new_branch} on top of {latest_branch}")
        create_new_branch(latest_branch, new_branch)

def create_new_branch(source_branch, new_branch):
    gl = gitlab.Gitlab(gitlab_api_url, private_token=private_token, api_version=4)
    project = gl.projects.get(project_id)
    source_branch_ref = project.branches.get(source_branch)
    project.branches.create({'branch': new_branch, 'ref': source_branch_ref.commit['id']})
    print(f"New branch '{new_branch}' created from '{source_branch}'.")

def main():
    parser = argparse.ArgumentParser(description="GitLab Branch Management")
    parser.add_argument("--api_url", required=True, help="GitLab API URL")
    parser.add_argument("--private_token", required=True, help="GitLab private token")
    parser.add_argument("--project_id", required=True, help="GitLab project ID")
    args = parser.parse_args()

    global gitlab_api_url, private_token, project_id
    gitlab_api_url, private_token, project_id = args.api_url, args.private_token, args.project_id

    all_branches = get_gitlab_branches(gitlab_api_url, private_token, project_id)
    pattern = r'bcp-2.\d+.x'
    matching_branches = find_matching_branches(all_branches, pattern)
    print(matching_branches)
    if matching_branches:
        get_latest_minor_branch(matching_branches)

if __name__ == "__main__":
    main()
