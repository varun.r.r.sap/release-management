# bcp-manifest

BCP Release Manifest Repository. Contains 

1. `manifest.yaml` - The Helm chart versions of Base Command Platform components 
2. `dependency_manifest.json` - A manifest file that defines a high-level SBOM in [CycloenDX](https://cyclonedx.org/) format.
    The set of items that are defined loosely identify the dependencies for thos version of BCP. This manifest is not to be used as an SBOM in true sense, but is intended for use to track dependency changes across BCP releases. 

## Creating a new BCP release 

BCP release will be following semver for tagging a release. The release cycle is managed by a pipeline which takes care of release creation, release promotion and qualification of a release. 

The pipeline takes the following variables as input arguments.

1. **COMMIT_SHA** - This maps the corresponding commit hash to the tag that is created.
2. **RELEASE_VERSION** - This argument denotes the tag upon which a new tag needs to be created.
3. **RELEASE_TYPE** - An dropdown denoting if a major, minor, patch or a pre-release needs to be created
4. **DRY_RUN** - A boolean variable by default set to true will not create a release but dry runs and outputs the release that is going to be created to stdout. Disable **DRY_RUN** to actually create a release.

### Creating a new pre-release|snapshot

Pre-releases are also known as snapshots. They are maintained in the format X.Y.Z-b.a. For instance 2.0.0-b.1

#### scenario 1 - when the RELEASE_VERSION specified doesn't have a pre-release.

If the mentioned **RELEASE_VERSION** doesn't have a pre-release, the pipeline takes care of creating a new pre-release for the same. For instance, if the **RELEASE_VERSION** input is *2.0.1* and if there is no pre-release, the pipeline creates a new tag with name *2.0.1-b.1* mapped to the **COMMIT_SHA**.

#### scenario 2 - when the RELEASE_VERSION specified already has a pre-release.

If the mentioned **RELEASE_VERSION** already has a pre-release, the pipeline increments the pre-release version of the latest tag. For instance, if the **RELEASE_VERSION** input is *2.0.0* and if the lastest pre-release available for it is *b.9* the pipeline creates a new pre-release *2.0.0-b.10*.

### Creating a new patch

Patch release are in the format X.Y.Z. Patch releases will be incrementing 'Z' part of the semver. For instance If the mentioned **RELEASE_VERSION** is 2.0.0, the pipeline checks the release with latest 2.0.Z and increments it to *2.0.(Z+1)*. Say if the latest release is *2.0.5*, the pipeline creates a *2.0.6* patch.

### Creating a new minor version update

Minor release are in the format X.Y.Z. Minor releases will be incrementing 'Y' part of the semver. For instance If the mentioned **RELEASE_VERSION** is *1.26.0*, the pipeline creates a new release *1.27.0*.

Some scenario's the pipeline will take care

1. If the user inputs the **RELEASE_VERSION** as *1.26.7*, the pipeline will know that the '.7' is insignificant and automatically creates a 1.27.0.
2. If the user inputs the **RELEASE_VERSION** as *1.25.0* and if there is a release *1.26.0* already exists, the pipeline will not try to create it. It will rather create a latest non existant minor release, in this case a *1.27.0* will be created.

### Creating a new major version update

Major releases are in the format X.Y.Z. Minor releases will be incrementing 'X' part of the semver. **RELEASE_VERSION** can be passed empty in the pipeline argument. The pipeline will fetch the latest major release available in the git repo and creates a new major release on top of it.

For instance, if the latest release is *2.1.6* the pipeline will create a new major release *3.0.0-b.1*

### Certifying a release

Release certifying is a means to identify if a release is validated by QA. 

1. For tagging a release as a QA certified release, got to code -> Tags 
2. In the tag list page at the end of each tag there will be a pipeline symbol.
3. On selecting, run the certify stage. 
4. It creates a new release with QA_Certified suffix which can then be consumed.

### Promoting a snapshot

For making a snapshot to a valid release, select the promote option from the dropdown. **RELEASE_VERSION** must be in the X.Y.Z format. The latest snapshot will be promoted to a valid semver release.

For instance if the latest snapshot is *2.0.0-b.10*, a new release with 2.0.0 with the ref pointing same as *2.0.0-b.10* will be created