#!/bin/bash

# Constants
GITLAB_API_URL="https://gitlab.com"
TOKEN="glpat-8L3BP_eWsrUzZcZ4trBY"
PROJECT_ID="53750415"
#COMMIT_SHA="5c03f6c59c414275a689b42df92ce96c500c8e95"

# Usage
usage() {
    cat <<-EOF
    Usage: bcp_release_cli [command] [args]

    Commands:
    get                  Gets the latest release available in bcp-release. No args needed.
    major                Generates a tag for the next major version and echoes to the screen. Supported args [-d, -t, -b]
    minor                Generates a tag for the next minor version and echoes to the screen. Supported args [-d, -t, -b]
    patch|hotfix         Generates a tag for the next patch version and echoes to the screen. Supported args [-d, -t, -b]
    pre-release|snapshot Generates a tag for a pre-release version and echoes to the screen. Supported args [-d, -t, -b]
    promote              Promotes a snapshot to a qualified release. Supported args [-d, -t]
    certify              Needs to be run by QA for marking it valid for prod release. Creates a new tag with the same ref with certified suffix. Supported args [-d, -c]
    help                 Echoes help commands

    Arguments:
    -d                   [dry-run] Dry-running and outputting the release to be created to stdout
                        Example: bcp_release_cli major -d -> creates a major release say 3.0.0
    -t                   [top-of-release] To create a release from another release
                        Example: bcp_release_cli minor -t 2.0.0 -> creates a minor release say 2.1.0. If 2.1.0 is available creates 2.2.0.
                        Example: bcp_release_cli patch -t 2.0.0 -> creates a patch release say 2.0.1. If 2.0.1 is available creates 2.0.2.
    -b                   [build] Create a custom build with a name
                        Example: bcp_release_cli patch -b skyhookfix -t 1.26.7 -> creates a release name 1.26.7+skyhookfix
    -c                   [certify] Create a certified tag for the mentioned release
                        Example: bcp_release_cli certify -c 2.0.0

    exit
EOF
    exit
}

# Helper functions

validate_build() {
    local build=$1
    if ! [[ "$build" =~ ^[0-9A-Za-z.-]*$ ]] || [[ "$build" =~ (^|\.)\. ]] || [[ "$build" =~ \.(\.|$) ]]; then
        echo "Error: build metadata is not valid."
        exit 1
    fi
}

# Version functions

version_parse_major() {
    echo "$1" | cut -d "." -f1
}

version_parse_minor() {
    echo "$1" | cut -d "." -f2
}

version_parse_patch() {
    echo "$1" | cut -d "." -f3 | sed 's/[-+].*$//g'
}

version_parse_pre_release() {
    echo "$1" | grep -oE '\-[a-zA-Z]+\.[0-9]+'
}

version_get() {
    local version=$(python3 git_ops.py --api_url "$GITLAB_API_URL" --private_token "$TOKEN" --project_id "$PROJECT_ID" search_tag "$1*")
    if [ -z "${version}" ]; then
        echo "Unable to find the mentioned release. Exiting !!!"
        exit 1
    else
        echo "${version}"
    fi
}

version_get_latest_tag() {
    local version=$(python3 git_ops.py --api_url "$GITLAB_API_URL" --private_token "$TOKEN" --project_id "$PROJECT_ID" get_latest_tag)
    if [ -z "${version}" ]; then
        echo "Unable to find the mentioned release. Exiting !!!"
        exit 1
    else
        echo "${version}"
    fi
}

version_major() {
    local build=${1:++$1}
    local version=$(version_get_latest_tag)
    local major=$(version_parse_major "${version}")
    local new=$((major + 1)).0.0${build}"-rc.1"
    version_do "$new"
}

version_minor() {
    local build=${1:++$1}
    local rel=$(echo "$top_of_release" | cut -d. -f1)
    local version=$(version_get "$rel")
    local major=$(version_parse_major "${version}")
    local minor=$(version_parse_minor "${version}")
    if [ -z "$version" ]; then
        echo "Unable to find the mentioned release. Exiting !!!"
        exit 1
    else
        local new=${major}.$((minor + 1)).0${build}
    fi
    version_do "$new"
}

version_patch() {
    local build=${1:++$1}
    local rel=$(echo "$top_of_release" | cut -d. -f1,2)
    local version=$(version_get "$rel")

    local major=$(version_parse_major "${version}")
    local minor=$(version_parse_minor "${version}")
    local patch=$(version_parse_patch "${version}")
    if [ -z "$version" ]; then
        echo "Unable to find the mentioned release. Exiting !!!"
        exit 1
    else
        local new=${major}.${minor}.$((patch + 1))${build}
    fi
    version_do "$new"
}

version_build() {
    local build=$1
    local version=$(version_get "$top_of_release")

    local major=$(version_parse_major "${version}")
    local minor=$(version_parse_minor "${version}")
    local patch=$(version_parse_patch "${version}")
    local pre_release=$(version_parse_pre_release "${version}")
    if [ -z "$version" ]; then
        echo "Unable to find the mentioned release. Exiting !!!"
        exit 1
    else
        local new=${major}.${minor}.${patch}${pre_release}+${build}
    fi
    version_do "$new"
}

version_pre_release() {
    local build=${1:++$1}
    local version=$(python3 git_ops.py --api_url "$GITLAB_API_URL" --private_token "$TOKEN" --project_id "$PROJECT_ID" search_tag "$top_of_release")
    if [ "$version" == "$top_of_release" ]; then
       echo "There is already a valid release. Snaphot creation is not possible when a release exists"
       exit 1
    fi

    if [ -z "$version" ]; then
        version=$top_of_release
    fi
    
    local major=$(version_parse_major "${version}")
    local minor=$(version_parse_minor "${version}")
    local patch=$(version_parse_patch "${version}")
    local original_string=$(version_parse_pre_release "${version}" | awk -F'.' '{print $1"."$2+1}') 

    if [ -z "$original_string" ]; then
        original_string="-rc.1"
    fi
    
    local new=${major}.${minor}.${patch}${original_string}${build}
    version_do "$new"
}

version_promote() {
    local version=$(version_get "$top_of_release")
    local major=$(version_parse_major "${version}")
    local minor=$(version_parse_minor "${version}")
    local patch=$(version_parse_patch "${version}")
    local pre_release=$(version_parse_pre_release "${version}")
    if [ -z "$pre_release" ]; then
        echo "There is no pre-release for the mentioned release. Exiting !!!"
        exit 1
    else
        local new=${major}.${minor}.${patch}
    fi
    version_do "$new"
}

certify_release() {
    local release=$1
    local version=$(python3 git_ops.py --api_url "$GITLAB_API_URL" --private_token "$TOKEN" --project_id "$PROJECT_ID" search_tag "$release")
    if [ -n "$version" ]; then
        version_do "${version}_QA_Certified"
    else
        echo "Unable to find the mentioned release. Exiting !!!"
        exit 1
    fi
}

version_do() {
    local new="$1"
    local version="$2"
    local cmd="git tag"

    if [ "$dryrun" == 1 ]; then
        echo "$new"
    else
        echo "Creating new tag $new with ref $COMMIT_SHA"
        python3 git_ops.py --api_url "$GITLAB_API_URL" --private_token "$TOKEN" --project_id "$PROJECT_ID" create_tag "$new" "$COMMIT_SHA"
    fi
}

# Parse args
action=
build=
top_of_release=
certify=
dryrun=0
while :; do
    case "$1" in
    -d | --dryrun)
        dryrun=1
        ;;
    -b)
        build=$2
        shift
        validate_build "$build"
        ;;
    -t)
        top_of_release=$2
        shift
        ;;
    -c)
        certify=$2
        shift
        ;;
    ?*)
        action=$1
        ;;
    *)
        break
        ;;
    esac
    shift
done

case "$action" in
get)
    version_get
    ;;
major)
    version_major "$build" "$top_of_release"
    ;;
minor)
    version_minor "$build" "$top_of_release"
    ;;
patch | bugfix | hotfix)
    version_patch "$build" "$top_of_release"
    ;;
build)
    [ -n "$build" ] || usage
    version_build "$build" "$top_of_release"
    ;;
pre-release | snapshot)
    version_pre_release "$build" "$top_of_release"
    ;;
promote)
    version_promote "$top_of_release"
    ;;
certify)
    certify_release "$certify"
    ;;
help)
    usage
    ;;
*)
    usage
    ;;
esac

